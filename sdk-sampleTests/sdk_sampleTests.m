//
//  sdk_sampleTests.m
//  sdk-sampleTests
//
//  Created by Kelvin Lau on 2016-03-17.
//  Copyright © 2016 ChargeSpot. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface sdk_sampleTests : XCTestCase

@end

@implementation sdk_sampleTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
