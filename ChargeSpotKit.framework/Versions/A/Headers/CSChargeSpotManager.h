//
//  CSChargeSpotManager.h
//  ChargeSpot
//
//  Created by Glen Yi on 2015-08-25.
//  Copyright (c) 2015 ChargeSpot. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

#import "CSChargeSpotEvent.h"
#import "CSGroupEvent.h"


@class CSGroup, CSChargeSpot;


/*!
 * @discussion  Device state definitions.
 */
typedef NS_ENUM(NSInteger, CSDeviceState) {
    /// Device state is unknown. Can happen if an error has occured.
    CSDeviceStateUnknown,
    /// Device bluetooth is disabled. Bluetooth is required to discover ChargeSpots.
    CSDeviceBluetoothDisabled,
    /// Device is not on a ChargeSpot.
    CSDeviceNotOnChargeSpot,
    /// Device is on a ChargeSpot but is not charging.
    CSDeviceOnChargeSpot,
    /// Device is charging and on a ChargeSpot.
    CSDeviceChargingOnChargeSpot,
    /// Device is charging on a ChargeSpot and has full battery.
    CSDeviceFullChargingOnChargeSpot,
};


/*!
 Device state block definition. Called when the device state was
 
 @param state CSDeviceState of current device.
 @param chargeSpot CSChargeSpot is nil if no ChargeSpot was found.
 @param error NSError will be nil if no errors were encountered.
 */
typedef void(^CSDeviceStateHandler)(CSDeviceState state, CSChargeSpot *chargeSpot, NSError *error);


NS_CLASS_AVAILABLE_IOS(7_0)


/*!
 *  @discussion This class discovers and dispatches CSChargeSpot and CSGroup events. Use the shared instance 
 *              of this class to register/unregister for ChargeSpot and Group events. Can also retrieve device 
 *              state which is defined in CSDeviceState typedef. App must set the applicationToken property before 
 *              using the class - this is typically done in the App Delegate.
 */
@interface CSChargeSpotManager : NSObject

///----------------------
/// @name Shared Instance
///----------------------

/*!
 *  @brief  Returns the shared static instance of CSChargeSpotManager.
 */
+ (instancetype)sharedManager;

///--------------------------
/// @name Class Configuration
///--------------------------

/*!
 *  @brief  Must be called before registering for ChargeSpot or Group events.
 */
- (void)setApplicationToken:(NSString *)applicationToken;

///-------------------------
/// @name Event Registration
///-------------------------

/*!
 *  @brief      Instructs the CSChargeSpotManager class to monitor for CSGroupEvents
 *
 *  @param options  NSDictionary of options describing how events are to be delivered.  Currently,
 *                  no options are supported
 *
 *  @return     Returns NO if beacon ranging is not available on the device. Otherwise YES.
 *
 *  @discussion UILocalNotifications will be dispatched whenever CSGroupEvents occur. To stop receiving
 *              CSGroupEvents, call the unregisterForGroupEvents method.
 *
 *  @see        CSGroupEvent
 *  @see        unregisterForGroupEvents
 */
- (BOOL)registerForGroupEventsWithOptions:(NSDictionary *)options;

/*!
 *  @brief      Instructs the CSChargeSpotManager class to monitor for CSChargeSpotEvents.
 *
 *  @param options  NSDictionary of options describing how events are to be delivered.  Currently,
 *                  no options are supported
 *
 *  @return     Returns NO if beacon ranging is not available on the device. Otherwise YES.
 *
 *  @discussion UILocalNotifications will be dispatched whenever CSChargeSpotEvents occur. To stop receiving
 *              CSChargeSpotEvents, call the unregisterForChargeSpotEvents method.
 *
 *  @see        CSChargeSpotEvent
 *  @see        unregisterForChargeSpotEvents
 */
- (BOOL)registerForChargeSpotEventsWithOptions:(NSDictionary *)options;

/*!
 *  @brief      Unregisters from future Group events.
 *
 *  @discussion Group events will no longer be monitored for.
 *
 *  @see        registerForGroupEventsWithOptions:
 */
- (void)unregisterForGroupEvents;

/*!
 * @brief       Unregisters from future ChargeSpot events.
 *
 * @discussion  ChargeSpot events will no longer be monitored for.
 *
 * @see         registerForChargeSpotEventsWithOptions:
 */
- (void)unregisterForChargeSpotEvents;

///-------------------------------
/// @name Monitoring Device Status
///-------------------------------

/*!
 *  @brief      Attempts to retrieve the device state.
 *
 *  @param completion   The block to handle state retrieval. Returns CSDeviceState, CSChargeSpot and NSError.
 *                      ChargeSpot will be nil if none were found. Error will be nil if no errors were encountered.
 *
 *  @return Returns NO if bluetooth is disabled, ranging is not available on the device or if already retrieving state.
 *          Otherwise YES.
 *
 *  @discussion The completion handler block is called when state retrieval is finished. Is handled
 *              asynchronously while it attempts to discover nearby ChargeSpots.
 */
- (BOOL)retrieveDeviceStateWithCompletion:(CSDeviceStateHandler)completion;

///----------------------------------
/// @name UILocalNotification Helpers
///----------------------------------

/*!
 *  @brief  Use this method to validate whether or not a local notification originated from this API
 *
 *  @param notification Any UILocalNotification object
 *
 *  @return Returns `YES` if the notification originated from this API.  Otherwise, returns `NO`
 */
- (BOOL)handleDidReceiveLocalNotification:(UILocalNotification *)notification;

/*!
 *  @brief      Extracts and returns a CSEvent object from a UILocalNotification
 *
 *  @param notification A UILocalNotification object, which has originated from this API
 *
 *  @return     Returns a CSEvent object (or one of its concrete subclasses)
 *
 *  @discussion Before calling this method, an app should verify the notification originated from this 
 *              API by utilizing the [CSChargeSpotManager handleDidReceiveLocalNotification:] method
 */
- (CSEvent *)eventFromLocalNotification:(UILocalNotification *)notification;

@end
