//
//  CSChargeSpotRegion.h
//  ChargeSpot
//
//  Created by Glen Yi on 2015-09-03.
//  Copyright (c) 2015 ChargeSpot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface CSChargeSpotRegion : NSObject

@property (copy, nonatomic) NSString *identifier;
@property (copy, nonatomic) NSUUID *uuid;
@property (copy, nonatomic) NSNumber *major;
@property (copy, nonatomic) NSNumber *minor;
@property (nonatomic) CLRegionState regionState;
@property (readonly, nonatomic) CLBeaconRegion *beaconRegion;

+ (instancetype)chargeSpotRegionFromDictionary:(NSDictionary *)dictionary;
+ (NSArray *)chargeSpotRegionsFromDictionaries:(NSArray *)dictionaries;

- (instancetype)initWithAPIDictionary:(NSDictionary *)dictionary;
- (instancetype)initWithBeaconRegion:(CLBeaconRegion *)beaconRegion;


@end
