//
//  CSChargeSpot.h
//  ChargeSpot
//
//  Created by Glen Yi on 2015-08-25.
//  Copyright (c) 2015 ChargeSpot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@class CSGroup;

/*!
 *  @discussion Represents a physical ChargeSpot location (i.e. ChargeSpot iBeacon)
 */
@interface CSChargeSpot : NSObject

///-----------------
/// @name Properties
///-----------------

/*!
 *  @brief  Returns the ID associated with this ChargeSpot (read-only)
 */
@property (strong, nonatomic, readonly) NSString *chargeSpotId;

/*!
 *  @brief  Returns the name associated with this ChargeSpot (read-only)
 */
@property (strong, nonatomic, readonly) NSString *name;

/*!
 *  @brief  Returns the underlying CSBeacon associated with this ChargeSpot (read-only)
 */
@property (strong, nonatomic, readonly) CLBeacon *beacon;

@end
