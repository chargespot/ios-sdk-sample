//
//  CSGroupEvent.h
//  ChargeSpotKit
//
//  Created by Daniel Stone on 2016-02-01.
//  Copyright © 2016 ChargeSpot Wireless Power Inc. All rights reserved.
//

#import "CSEvent.h"
#import "CSGroup.h"


/*!
 * @discussion  Group event definitions.
 *
 * @see         [CSGroupEvent type]
 */
typedef NS_ENUM(NSInteger, CSGroupEventType) {
    /// Device entered a ChargeSpot group.
    CSEventEnteredGroup = 1,
    /// Device exited a ChargeSpot group.
    CSEventExitedGroup
};


/*!
 * @discussion  A CSGroupEvent will be generated whenever an iOS device enters a region containing one or 
 *              more ChargeSpot beacons.  For additional information about events, and how they are 
 *              dispatched, please see the CSEvent documentation.
 */
@interface CSGroupEvent : CSEvent

/*!
 *  @brief      Returns the CSGroupEventType of the current event (read-only)
 *
 *  @discussion This property provides additional information about the type of event which has occurred
 */
@property (readonly) CSGroupEventType type;

/*!
 *  @brief      Returns the CSGroup associated with the current event (read-only)
 */
@property (readonly) CSGroup *group;

@end
