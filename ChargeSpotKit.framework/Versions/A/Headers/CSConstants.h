//
//  CSConstants.h
//  Pods
//
//  Created by Daniel Stone on 2016-01-29.
//
//

#import <Foundation/Foundation.h>

extern NSString *const CSKeyEventType;
extern NSString *const CSKeyEventID;
extern NSString *const CSKeyChargeSpot;
extern NSString *const CSKeyHooks;
extern NSString *const CSKeyError;

extern NSString *const CSEventGroup;
extern NSString *const CSEventChargeSpot;

extern NSString *const CSNotificationTypeKey;
extern NSString *const CSNotificationEventKey;
extern NSString *const CSNotification;
