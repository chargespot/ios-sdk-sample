//
//  CSChargeSpotEvent.h
//  ChargeSpotKit
//
//  Created by Daniel Stone on 2016-02-01.
//  Copyright © 2016 ChargeSpot Wireless Power Inc. All rights reserved.
//

#import "CSEvent.h"
#import "CSChargeSpot.h"


/*!
 * @discussion  ChargeSpot event definitions.
 *
 * @see         [CSChargeSpotEvent type]
 */
typedef NS_ENUM(NSInteger, CSChargeSpotEventType) {
    /// Device is charging and on a ChargeSpot.
    CSEventStartedChargingOnChargeSpot = 1,
    /// Device stopped charging on a ChargeSpot.
    CSEventStoppedChargingOnChargeSpot
};


/*!
 *  @discussion A CSChargeSpotEvent will be generated whenever an iOS device begins, or finishes charging on a
 *              ChargeSpot.   For additional information about events, and how they are dispatched, please see 
 *              the CSEvent documentation.
 */
@interface CSChargeSpotEvent : CSEvent

/*!
 *  @brief      Returns the CSChargeSpotEventType of the current event (read-only)
 *
 *  @discussion This property provides additional information about the type of event which has occurred
 */
@property (readonly) CSChargeSpotEventType type;

/*!
 *  @brief  Returns the CSChargeSpot associated with the current event (read-only)
 */
@property (readonly) CSChargeSpot *chargeSpot;

@end
