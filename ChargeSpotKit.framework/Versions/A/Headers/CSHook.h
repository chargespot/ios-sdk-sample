//
//  CSHook.h
//  ChargeSpot
//
//  Created by Glen Yi on 2015-08-25.
//  Copyright (c) 2015 ChargeSpot. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 * @discussion  Distinguishes between different types of hooks
 */
typedef NS_ENUM(NSInteger, CSHookType) {
    /// Generic hook
    CSHookTypeNone = 0,
    /// Promotion hook
    CSHookTypePromotion = 1
};

/*!
 *  @discussion A hook provides actionable information associated with a CSEvent
 */
@interface CSHook : NSObject

///-----------------
/// @name Properties
///-----------------

/*!
 *  @brief  Returns the ID associated with this hook (read-only)
 */
@property (strong, nonatomic, readonly) NSString *hookId;

/*!
 *  @brief  Returns the name associated with this hook
 */
@property (strong, nonatomic, readonly) NSString *name;

/*!
 *  @brief  Returns the CSHookType of this hook
 */
@property (nonatomic, readonly) CSHookType type;

/*!
 *  @brief  Returns a URL associated this hook
 *
 *  @discussion Often, applications will want to instruct the iOS device to open this URL
 */
@property (strong, nonatomic, readonly) NSURL *actionURL;

/*!
 *  @brief  Returns the delay, in seconds, between the associated event occurring, and the event being delivered
 */
@property (nonatomic, readonly) NSTimeInterval actionDelay;

/*!
 *  @brief  Returns the content shown in the UILocalNotification body
 */
@property (strong, nonatomic, readonly) NSString *notificationText;

@end
