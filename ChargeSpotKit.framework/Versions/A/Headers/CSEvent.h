//
//  CSEvent.h
//  ChargeSpotKit
//
//  Created by Daniel Stone on 2016-02-01.
//  Copyright © 2016 ChargeSpot Wireless Power Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSHook.h"

/*!
 * @discussion  Base class for more specific ChargeSpot events.  These events will be delivered to your 
 *              [UIApplicationDelegate](https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIApplicationDelegate_Protocol/index.html)
 *              as [UILocalNotifications](https://developer.apple.com/library/ios/documentation/iPhone/Reference/UILocalNotification_Class/).
 *              To retrieve a CSEvent, see the [CSChargeSpotManager eventFromLocalNotification:] method
 */
@interface CSEvent : NSObject

/*!
 * @brief   Returns the hook associated with the event (read-only)
 *
 * @see     CSHook
 */
@property (readonly) CSHook *hook;

- (BOOL)isEqualToEvent:(CSEvent *)event;
- (BOOL)isEqual:(id)object;

@end
