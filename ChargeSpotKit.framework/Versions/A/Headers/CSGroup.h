//
//  CSGroup.h
//  ChargeSpot
//
//  Created by Glen Yi on 2015-08-25.
//  Copyright (c) 2015 ChargeSpot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "CSChargeSpotRegion.h"

/*!
 *  @discussion A CSGroup represents a region containing at least one CSChargeSpot
 */
@interface CSGroup : CSChargeSpotRegion <MKAnnotation>

///-----------------
/// @name Properties
///-----------------

/*!
 *  @brief  Returns the ID associated with the group (read-only)
 */
@property (strong, nonatomic, readonly) NSString *groupId;

/*!
 *  @brief  Returns the name of the group (read-only)
 */
@property (strong, nonatomic, readonly) NSString *name;

/*!
 *  @brief  Returns the street address of the group (read-only)
 */
@property (strong, nonatomic, readonly) NSString *address;

/*!
 *  @brief  Returns the URL of an image associated with the group (read-only)
 */
@property (strong, nonatomic, readonly) NSURL *imageURL;

/*!
 *  @brief  Returns the GPS coordinates of the group (read-only)
 */
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

/*!
 *  @brief  Returns the number of ChargeSpots located within the group (read-only)
 */
@property (nonatomic, readonly) NSUInteger chargeSpotCount;

/*!
 *  @brief  Returns an array of ChargeSpots located within the group (read-only)
 */
@property (strong, nonatomic, readonly) NSArray *chargeSpots;

///----------------------
/// @name Utility Methods
///----------------------

/*!
 *  @brief  Calculates and returns the distance between this CSGroup and the provided coordinates
 *
 *  @param coordinates  A pair of GPS coordinates
 *
 *  @return Returns the distance between the two pairs of coordinates in km
 */
- (CGFloat)distanceFromCoordinates:(CLLocationCoordinate2D)coordinates;

@end
