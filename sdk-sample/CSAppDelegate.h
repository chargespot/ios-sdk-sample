//
//  CSAppDelegate.h
//  sdk-sample
//
//  Created by Kelvin Lau on 2016-03-17.
//  Copyright © 2016 ChargeSpot. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

