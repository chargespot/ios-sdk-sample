//
//  DeviceStateViewController.m
//  sdk-sample
//
//  Created by Kelvin Lau on 2016-03-17.
//  Copyright © 2016 ChargeSpot. All rights reserved.
//

#import "DeviceStateViewController.h"

#import <ChargeSpotKit/ChargeSpotKit.h>
#import <ChargeSpotKit/CSConstants.h>

@interface DeviceStateViewController () <CLLocationManagerDelegate>
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation DeviceStateViewController

#pragma mark - ViewController Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerForNotifications];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    CLAuthorizationStatus locationStatus = [CLLocationManager authorizationStatus];
    if (locationStatus == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unregisterNotifications];
}

- (void)registerForNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateDeviceState)
                                                 name:CSEventChargeSpot
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEnterGroupRegion)
                                                 name:CSEventGroup
                                               object:nil];
}

- (void)unregisterNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

# pragma mark - Helper Methods

/*!
 @brief Initializes process to update device and chargespot states
 */
- (void)updateDeviceState {

    // Polling device for charge state
    [[CSChargeSpotManager sharedManager] retrieveDeviceStateWithCompletion:^(CSDeviceState state, CSChargeSpot *chargeSpot, NSError *error) {
        // Callback with chargespot and device state
    }];
}

/*!
 @brief Callback for when a group event occurs
 */
- (void)didEnterGroupRegion {
    // Trigger group event
}

/*!
 @brief Registers for chargespot and group events and notifications
 @discussion Uncomment group if testing for entering/exiting regions
 */
- (void)startChargeSpotManager {
    // ChargeSpot event monitoring
    [[CSChargeSpotManager sharedManager] registerForChargeSpotEventsWithOptions:nil];

    
    // Group event monitoring
    [[CSChargeSpotManager sharedManager] registerForGroupEventsWithOptions:nil];

}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        [self startChargeSpotManager];
    }
}

@end
