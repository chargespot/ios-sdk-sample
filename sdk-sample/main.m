//
//  main.m
//  sdk-sample
//
//  Created by Kelvin Lau on 2016-03-17.
//  Copyright © 2016 ChargeSpot. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CSAppDelegate class]));
    }
}
