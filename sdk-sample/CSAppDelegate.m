//
//  CSAppDelegate.m
//  sdk-sample
//
//  Created by Kelvin Lau on 2016-03-17.
//  Copyright © 2016 ChargeSpot. All rights reserved.
//

#import "CSAppDelegate.h"
#import <ChargeSpotKit/ChargeSpotKit.h>
#import <ChargeSpotKit/CSConstants.h>

@interface CSAppDelegate ()
@end

@implementation CSAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Application Token in Info.plist. Replace with one that is valid for your SDK
    NSString *CSAppToken = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CSAppToken"];
    
    // Init ChargeSpotManager
    [[CSChargeSpotManager sharedManager] setApplicationToken:CSAppToken];

    return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    // Check if the notification originated from ChargeSpotKit
    if ([[CSChargeSpotManager sharedManager] handleDidReceiveLocalNotification:notification]) {
        CSEvent *event = [[CSChargeSpotManager sharedManager] eventFromLocalNotification:notification];
        
        // Redirect browser to action URL - use dispatch_async to allow app to come to foreground
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] openURL:event.hook.actionURL];
        });
        
        // Check if the event is a CSChargeSpotEvent
        if ([event isKindOfClass:CSChargeSpotEvent.class]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CSEventChargeSpot
                                                                object:self
                                                              userInfo:@{CSNotificationEventKey: event}];
            
        // Otherwise, check if the event is a CSGroupEvent
        } else if ([event isKindOfClass:CSGroupEvent.class]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:CSEventGroup
                                                                object:self
                                                              userInfo:@{CSNotificationEventKey: event}];
        }
    }
}

@end
